---
theme: gaia
_class: lead
paginate: true
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.jpg')
---

![bg left:40% 80%](https://logotyp.us/file/zillow.svg)

# **Free and Open Source Software at Zillow Group**


---

# Who are you?
- I am a College of Charleston Alumni, graduated in 2000
- All the companies I have worked for have used open source heavily.
- I currently work for Zillow Group.
- [@twonds](https://github.com/twonds)
- email: christopher.zorn@gmail.com

---

# What are you talking about?

- 'Turn on the lights' for technology used by ZG.
- Try not to be boring.
- Trigger your curiosity to explore technology used by ZG.
- Ask questions!

---

# What is Zillow Group?
- [Zillow Group](https://www.zillowgroup.com/) is best known for the Zillow brand and zillow.com.
- It also has many other brands. [Trulia](https://www.trulia.com/), [Hotpads](https://hotpads.com/), [StreetEasy](https://streeteasy.com/), etc.
- ZG has real estate businesses:
  - Premier Agent, Zillow Offers, Zillow Rentals, Zillow Home Loans, Zillow Closing Services

- Zillow, like many other tech companies, use and contribute to free and open source software.

---

# Why does ZG participate in open source?

 - Developing technology efficiently
   - Multiplies developer impact
   - Collaboration on mission-critical projects
   - High-quality open source code
 - Growing and maintaining talented engineering teams
 - Supports a healthly technology ecosystem

---

# How does FOSS multiply developer impact?

 - The most common motivator for participating in open source is engineering efficiency.
 - Problems are already solved.
 - Consuming open source projects means you don’t have to reinvent the wheel.
 - Creating open source projects means your wheel can be used by others.

---

# How does collaboration improve mission critical projects?

- FOSS encourages public collaboration.
- It allows us to contribute improvements to projects critical to ZG's success.
- Collaboration increases diversity of thought, typically increasing quality.

---

# How does FOSS produce higher quality code?

- Popular FOSS projects tend to be higher quality than software developed by small, internal teams.
- They benefit from many contributors with diverse perspectives, backgrounds, and experiences.
- Diversity improves most aspects of software development, strengthening design, testing and usability.

---

# How does it help grow and maintain our talented engineering teams?

- Using known or standard methods, FOSS reduces new engineers learning time.
- Reduces overhead and toil of operation and maintenance.
- Participating in FOSS exposes software engineering candidates to ZG technology.
- Connects outside engineers with ZG software engineers.
- Sends positive signals that ZG takes FOSS seriously.

---

# Why does FOSS fit in with ZG values?

- FOSS aligns a few of ZG’s [core values](https://www.zillowgroup.com/about-us/story/):
  - **Do the right thing**: ZG benefits greatly from the FOSS community, and we should give back where we can.
  - **Turn on the lights**: ZG works to increase transparency within the real estate industry to empower our users. This should extend to our software.
  - **ZG is a team sport**: We’re at our best when we work together and value what each person brings to the table. Participating in FOSS expands our team and the benefits that come with it.


---

# How does ZG participate in FOSS?

- Consumes by using thousands of open source projects to build fast, scalable, and reliable technology.
- Contributes by submitting code and other improvements to the open source projects we use.
- Creates by releasing our own open source projects for others to use.

---

# What FOSS technology does ZG consume?

- Guess?


---

# What FOSS technology does ZG consume?

It is actually difficult to measure how many FOSS projects we use.

---

# Languages
  - [python](https://www.python.org/)
  - [javascript](https://nodejs.org/)
  - [java](https://www.java.com/)
  - [php](https://www.php.net/)
  - [go](https://golang.org/)

---
# Frameworks
  - [React](https://reactjs.org/)
  - [FastAPI](https://fastapi.tiangolo.com/)
  - [nextjs](https://nextjs.org/)
  - [nestjs](https://nestjs.com/)
  - [spring](https://spring.io/projects/spring-boot)

---

# Databases
 - [Postgresql](https://www.postgresql.org/)
 - [MySQL](https://www.mysql.com/)
 - [Redis](https://redis.io/)

# Caching/Indexes
 - [Redis](https://redis.io/)
 - [Varnish](https://varnish-cache.org/)
 - [memcached](https://memcached.org/)
 - [Solr](https://lucene.apache.org/solr/)

---

# Documentation
 - [OpenAPI (aka Swagger)](https://swagger.io/specification/)

# Source Control, CI/CD, Operation
 - [Gitlab](https://gitlab.com/gitlab-org/gitlab)
 - [Git](https://git-scm.com/)
 - [Tilt](https://tilt.dev/)
 - [Docker](https://www.docker.com/)
 - [Terraform](https://www.terraform.io/)
 - [Kubernetes](https://kubernetes.io/)

---
# Mobile
 - I don't know!

# Monitoring
 - [OpenTelemetry](https://opentelemetry.io/)
 - [Graphite](https://graphiteapp.org/)
 - [Grafana](https://grafana.com/)

---

# How does ZG contribute back to FOSS?

On and off the clock, ZG engineers contribute back to the open source community by submitting bug fixes, features, issues, documentation, and maintenance to non-ZG projects.

---

# What projects does ZG contribute to?

- [Metaflow](https://metaflow.org/)
- [Luminaire](https://github.com/zillow/luminaire)
- [Lerna](https://github.com/lerna/lerna)
- [OpenTelemetry](https://opentelemetry.io/)
- [Grafana](https://grafana.com/)
- [Kubernetes](https://kubernetes.io/)
- [Solr Operator](https://github.com/apache/lucene-solr-operator)
- Many others

---

# What FOSS project's does ZG create?

- [React-slider](https://github.com/zillow/react-slider) - Accessible, CSS agnostic, slider component for React.
- [Luminaire ](https://github.com/zillow/luminaire)- python package that provides ML driven solutions for monitoring time series data.
- [ctds](https://github.com/zillow/ctds) - Python DB-API 2.0 library for MS SQL Server
- Github: https://github.com/zillow/?q=&type=source&language=

---


# What about your team?

- My team: Devex Platform
- Responsible for delivery of workloads, applications and services.
- Our job is to make it easy for engineering teams to deploy, scale and operate their solutions.

---

# What projects does my team consume?

- Similar to the list already given.
- Python and golang
- Kubernetes
  - Kustomize
  - Tools
  - Operators

---

# How does my team contribute?

- Kubernetes - release and test teams
- Solr k8s Operator
- Many smaller contributions
- Small example: https://github.com/python-gitlab/python-gitlab/pull/1069

---

# What projects does my team create?

- Not many but maybe soon!
- [hyper-kube-config](https://github.com/zillow/hyper-kube-config)
- [dinghy ping](https://github.com/silvermullet/dinghy-ping)

---

# Can you demonstrate a project?

 - dinghy ping!


---


# How can ZG increase FOSS participation?

- Consuming is ZG’s primary mode of open source participation.
- The biggest participation opportunities involve contributing and creating.

---

# How is ZG improving guidelines around FOSS participation?

- A small group of passionate, self-motivated engineers is responsible for the majority of ZG’s open source participation.
- We want to make it easier for everyone to participate.
- The most efficient way to facilitate participation is to improve and ratify our open source participation guidelines.

---

# How can ZG change culture and normalize FOSS participation?

- Some teams strive toward an “open source by default” model of development.
- But the overhead of “open sourcing” internal projects is avoided entirely but should be prioritized similar to internal tasks.
- All projects depend on OSS, upstream contributions should also be prioritized like any internal development task.

---
# What are the risks of using FOSS for ZG?

- Legal
- Security

---

# Legal risks
  - Legal risks of FOSS include potential infringement and license restrictions.
    - Inadvertently violating terms.
    - Adopting a project whose license requires us to release sensitive code.

---
# Security risks

- Security risks of FOSS include the following:
    - Introducing malware or vulnerabilities in our technology via dependencies.
    - Inadvertently releasing vulnerabilities ourselves.

---

# How does ZG mitigate risks of FOSS?

- Legal and security teams review certain kinds of open source participation, e.g., when open sourcing an internal project.
- However, as open source participation grows, there will be increased demand for legal and security reviews.

---
# How can ZG maintain or reduce the review workload?
 - Automate reviews
 - Reduce reviews
 - Simplify reviews

---

# Automate reviews
   - ZG can use scanning tools to automatically review open source licenses.
   - ZG already scans for security vulnerabilities in projects and docker images.
---

# Reduce reviews
   - We need to define which types of open source participation require manual review.
   - Possibly avoid small contribution reviews.
---
# Simplify reviews
- ZG has plans to leverage ServiceNow workflows to help manage the manual review process.


---

# What did we get out of this?

- FOSS tech overview at ZG
- Use, contribute and study open source!
- Reading and understanding code is the best way to learn!
- You are more than welcome to reach out to me for questions.

---

# Is Zillow hiring?

- Yes!
- Interns
- No entry level on my team just yet
- https://www.zillow.com/careers/university/
- https://www.zillow.com/careers/
---

# Questions?

 - Christopher Zorn
 - christopher.zorn@gmail.com
 - @twonds


# References and resources

- Github page: https://github.com/zillow/
- Zillow Tech Hub: https://www.zillow.com/tech/
