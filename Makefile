NAME=asphalt
PACKAGE=gitlab.zgtools.net/zg-container-platform/services/asphalt

BUILDDIR=build
DOCSDIR=docs

VERSION?=dev

LDFLAGS=-ldflags "-X ${PACKAGE}/pkg/version.Version=${VERSION}"
BUILDCMD=go build -v ${LDFLAGS}


${BUILDDIR} : swagger ${BUILDDIR}/${NAME} ${BUILDDIR}/cmd/writeservice ${BUILDDIR}/cmd/getservicemetadata


${BUILDDIR}/${NAME} :
	${BUILDCMD} -o $@ .

${BUILDDIR}/cmd/getservicemetadata :
	${BUILDCMD} -o $@ ./cmd/getservicemetadata

${BUILDDIR}/cmd/writeservice :
	${BUILDCMD} -o $@ ./cmd/writeservice

swagger:
	go get -u github.com/swaggo/swag/cmd/swag
	swag init --output ${DOCSDIR}

test:
	go test -count=1 ./...

run: build
	${BUILDDIR}/${NAME}

vet:
	go vet ./...

fmt:
	go fmt ./...

clean:
	rm -rf ${BUILDDIR} ${DOCSDIR}
