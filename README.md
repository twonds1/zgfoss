# A REST API service demoing FOSS technology used at Zillow Group

## Github

- https://github.com/zillow
- https://github.com/zillow?q=&type=source&language=

## Blog post on Open Source at Zillow Group

https://www.zillow.com/tech/open-source-at-zillow-group/
