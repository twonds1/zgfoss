FROM golang:1.14 AS build

ARG version=dev

ENV CGO_ENABLED=0

WORKDIR /go/src/asphalt
COPY . .
RUN make build VERSION=${version}
RUN make test

FROM scratch

COPY --from=build /go/src/asphalt/build/asphalt /
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=redboxoss/scuttle:1.3.0 /scuttle /usr/bin/scuttle

ENTRYPOINT ["/usr/bin/scuttle", "/asphalt"]
