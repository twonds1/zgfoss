# Free and Open Source at Zillow Group


## Introduction

### Who am I?
- I am a College of Charleston Alumni, graduated in 2000
- All the different companies I have worked for have used open source heavily
- Some have contributed heavily
- Some have contributed very little
- I currently work for Zillow Group

- This is about FOSS at Zillow Group but feel free to ask any question at any time
  - Interupt and ask questions

### What is Zillow Group?
- Zillow Group is the company that is best known for the Zillow brand and zillow.com product
- It also has many other brands. Trulia, Hotpads, StreetEasy, etc
- It has real estate businesses. Premier Agent, Zillow Offers, Zillow Rentals, Zillow Home Loans, Zillow Closing Services
- It has real estate software. Mortech, Dotloop, Bridge Interactive
- It is a large tech company

- Zillow, like many other tech companies, use and contribute to free and open source software.

## Open Source At Zillow Group
 - https://github.com/zillow
 - https://www.zillow.com/tech/open-source-at-zillow-group/

### Why does ZG participate in open source?
 - Multiplies developer impact
 - Allows for learning and evolving
 - Mission critical project collaboration ?
 - High quality code
 - Auditable code
 - Increased security
 - Growing and maintaining talented engineers

### How does ZG participate in open source?

#### Consume

- Languages
  - python
  - php
  - javascript
  - java

- Databases
 - postgresql
 - mysql
 - Redis

- Caching
 - Redis
 - Varnish
 - memcache

- Documentation

- CI/CD
 - Gitlab
 - git

#### Contribute

#### Create

## Demo slides

- Show docker, tilt and k8s
- Discuss how it fits with devex team
- Show gitlab?
- 

## Open Source on devex platform team

- Languages
  - python
  - javascript/node
  - java
  - golang

- CI/CD
 - Gitlab
 - git

- Over view of contributions

## Demo project

- Show typical workflow
- Design
- Test/Implement
- Test
- Deploy

## Questions?
